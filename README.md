# ROR Learning Process

This repository stores rails related tutorial that I followed for my ROR learning process. Topics being covered are as follow:

	1. Gems / Bundler / Upgrade / Package
	2. Model / ActiveRecord / Association
	3. Seeding / Migration / Backup
	4. Query
	5. Controller / Routing
	6. Response Format / JSON


## Gems / Bundler / Upgrade / Package

### References
	- http://railsapps.github.com/rails-3-1-example-gemfile.html
	- http://railscasts.com/episodes/201-bundler


## Model / ActiveRecord / Association

### References
	- http://ar.rubyonrails.org/
	- http://guides.rubyonrails.org/association_basics.html
	- http://api.rubyonrails.org/classes/ActiveRecord/Associations/ClassMethods.html
	- http://rubylearning.com/satishtalim/ruby_activerecord_and_mysql.html


## Seeding / Migration / Backup

### References
	- http://guides.rubyonrails.org/migrations.html
	- http://wiki.rubyonrails.org/rails/pages/UnderstandingMigrations


## Query

### References
	- http://guides.rubyonrails.org/active_record_querying.html



## Controller / Routing

### References
	- http://guides.rubyonrails.org/routing.html


## Response Format / JSON
	- http://guides.rubyonrails.org/form_helpers.html
	- http://guides.rubyonrails.org/layouts_and_rendering.html


## Running The Application

There is database dependency for this application hence you will need to configure the file `config/database.yml` and once done execute rake command

	rake db:migrate

Once done run the command

	rails s
