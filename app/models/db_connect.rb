require 'rubygems'
require 'active_record'

ActiveRecord::Base.establish_connection(
    :adapter => "mysql2",
    :host => "localhost",
    :database => "students",
    :username => "root",
    :password => 'root'
)

class Rubyist < ActiveRecord::Base
end

Rubyist.create(:name => 'Luc Juggery', :city => "Nashville, Tenessee")
Rubyist.create(:name => 'Sunil Kelkar', :city => "Pune, India")
Rubyist.create(:name => 'Adam Smith', :city => "San Fransisco, USA")

participant = Rubyist.first
puts %{#{participant.name} stays in #{participant.city}}

Rubyist.first.destroy