=begin
CREATE TABLE companies(
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  client_of INT(11),
  name VARCHAR(255),
  type VARCHAR(100),
  PRIMARY KEY (id)
);

CREATE TABLE people (
  id int(11) unsigned NOT NULL auto_increment,
   name text,
   company_id text,
   PRIMARY KEY  (id)
);
=end

require 'active_record'

class Company < ActiveRecord::Base
  has_many :people, :class_name => "Person"
end

class Firm < Company
  has_many :clients

  def people_with_all_clients
    clients.inject([]) { |people, client| people + client.people }
  end
end

class Client  < Company
  belongs_to :firm, :foreign_key => "client_of"
end

class Person < ActiveRecord::Base
  belongs_to :company
end

ActiveRecord::Base.establish_connection(
    :adapter => "mysql2",
    :host => "localhost",
    :username => "root",
    :password => "root",
    :database => "ruby"
)

# create a new entry at `companies`
firm = Firm.new("name" => "Next Angle")
firm.save

client = Client.new("name" => "37signals", "client_of" => firm.id)
client.save